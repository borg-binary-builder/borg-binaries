#!/usr/bin/env sh

set -ex

# Usage: ./extract.sh <image> <binary_save_location>
# Usage: ./extract.sh <arch> [<version>] [<binary_save_location>]

extract_binary_from_image() {
    # Create a temporary container and extract the borg binary.
    TMP_CONTAINER_ID=$(docker create "${DOCKER_IMAGE}")
    # Do NOT print to stdout and write to file with " - > "!!
    docker cp -L $TMP_CONTAINER_ID:/usr/bin/borg "${BORG_BINARY}"
    docker rm -v $TMP_CONTAINER_ID >/dev/null
    chmod +x "${BORG_BINARY}"
}

DOCKER_IMAGE=${1}
BORG_BINARY=${2}
# Looks like we've been passed an image.
if [  "${DOCKER_IMAGE}" ] && docker inspect "${DOCKER_IMAGE}" >/dev/null 2>&1; then
    if [ -z "${BORG_BINARY}" ]; then
        echo "Usage: ./extract.sh <image> <binary_save_location>"
        echo "No binary save location provided"
        exit 1
    fi

    extract_binary_from_image
    exit 0
fi

BORG_ARCH=${1}
BORG_VERSION=${2:-1.1.14}
BORG_BINARY=${3:-borg-${BORG_VERSION}-${BORG_ARCH}}

DOCKER_IMAGE=build

if [ -z "${BORG_ARCH}" ]; then
    echo "Usage: ./extract.sh <image> <binary_save_location>"
    echo "Usage: ./extract.sh <arch> [<version>] [<binary_save_location>]"
    echo "No docker image or arch provided"
    exit 1
fi

if ! docker inspect "${DOCKER_IMAGE}" >/dev/null 2>&1; then
    echo "Invalid docker image or arch and version provided!"
    echo "Create the image first, using './build.sh'"
    exit 1
fi

extract_binary_from_image
