# Borg Binary Builder

Build [BorgBackup] binaries on any platform, for any architecture, using docker.

## Usage

### Build image

`./build.sh <arch> [<version>] [<image>]`

See the [list of available architectures] ([on GitHub][available-architectures-github])

*Examples:*
- Build version `1.1.14` for `aarch64` with default image name (`borg:1.1.14-aarch64`)

`./build.sh aarch64 1.1.14`

- Build version `1.1.5` for Raspberry Pi 3 with custom image name (`borg:1.1.5-rpi3`)

`./build.sh raspberrypi3 1.1.5 borg:1.1.5-rpi3`

### Extract borg binary

`./extract.sh <image> <binary_save_location>`
`./extract.sh <arch> [<version>] [<binary_save_location>]`

**Note:** This will overwrite the file at `binary_save_location` if it already exists!

*Examples:*
- Extract the borg `1.1.14` binary for `aarch64` to `borg-aarch64` in the current folder

`./extract.sh aarch64 1.1.14 borg-aarch64`

- Extract the borg binary from the `borg:1.1.5-rpi3` image to `/homes/lemoi/borg`

`./extract.sh borg:1.1.5-rpi3 /homes/lemoi/borg`

### Build base image

To speed up the building process, certain architectures have a prebuilt base image with the development requirements already installed.

These base images are based on BorgBackup versions:
- 1.1.14 *(for versions 1.1.14 and later)*
- 1.1.10 *(for versions 1.1.10 to 1.1.13)*
- 1.1.0 *(for versions 1.1.0 to 1.1.9)*

Adding a new architecture (of a specific version) to the base image list and pushing it to docker hub:

`./build-base.sh <arch> [<version>]`

*Examples:*
- Build base image for `aarch64` set at version 1.1.14:

`./build-base.sh aarch64 1.1.14`

## How this works

### Build

The `build.sh` script makes use of the QEMU cross building provided in the used Balena images. For more details, check the [Balena docs].

Then, the docker image is built for the requested architecture.

This is basically done by following the flow of setting up a [BorgBackup development environment] and [creating the standalone binary]. (For more info on this, just have a look at the `Dockerfile` and `docker-setup.sh` files)

To use the base images created by `build-base.sh` instead of the normal build image, you can set the `BBB_HOT` environment variable to any value before executing `build.sh`. This starts off the build with the prebuilt base image, speeding up the build tremendously.

`BBB_HOT=yep ./build.sh ...`

Builds are based on the Python 3.8 images on Debian Stretch (1.1.14 and later) and the Python 3.5 images on Debian Jessie (1.1.0 to 1.1.13).

### Extract

The `extract.sh` script extracts the built binary from the image created using `build.sh`.

This basically creates a temporary container from the built image for your selected architecture and version (or custom image name) and writes the binary out to the file path you passed as a parameter.

## Verify PGP Signature

All automated binary builds and signatures can be found here:
- https://borg.bauerj.eu

Using version `1.1.14` for `armv7` as an example:

1. Simply download the desired binary (`borg-1.1.14-armv7`) and it's corresponding signature file (`borg-1.1.14-armv7.asc`).
2. Next we have to import the official PGP public key needed for verification (which you can find in this repo, [here](https://gitlab.com/borg-binary-builder/borg-binaries/-/raw/master/borg-binary-builder.public.asc)):
    - `gpg --import borg-binary-builder.public.asc`
3. Then verify the downloaded binary with it's signature file:
    - `gpg --verify borg-1.1.14-armv7.asc borg-1.1.14-armv7`
4. Whoop! 🎉

## License

Borg Binary Builder is licensed under the terms of the [WTFPL]. See the [LICENSE] file.

[BorgBackup]: https://github.com/borgbackup/borg/ "BorgBackup on GitHub"
[BorgBackup development environment]: https://borgbackup.readthedocs.io/en/stable/development.html#building-a-development-environment "BorgBackup development environment"
[creating the standalone binary]: https://borgbackup.readthedocs.io/en/stable/development.html#creating-standalone-binaries "Create standalone binary"
[list of available architectures]: https://www.balena.io/docs/reference/base-images/base-images/#balena-base-images "List of available Architectures"
[available-architectures-github]: https://github.com/balena-io-library/base-images/tree/master/balena-base-images/python "List of available Architectures on GitHub"
[Balena docs]: https://www.balena.io/docs/reference/base-images/base-images/#building-arm-containers-on-x86-machines "Building ARM Containers on x86 Machines"
[WTFPL]: http://www.wtfpl.net/about/ "Do What the Fuck You Want to Public License"
[LICENSE]: https://gitlab.com/borg-binary-builder/borg-binaries/blob/master/LICENSE "View License"
