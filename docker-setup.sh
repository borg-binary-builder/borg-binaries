#!/usr/bin/env sh

BORG_ARCH=${1}
BORG_VERSION=${2:-1.4.0}

set -x

export PATH="$HOME/.pyenv/bin:$HOME/.pyenv/shims:$PATH"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
export PYI_STATIC_ZLIB=1
export PYINSTALLER_COMPILE_BOOTLOADER=1

cd /
# fetch borg source code
git clone https://github.com/borgbackup/borg.git && cd borg
git fetch --all
git checkout ${BORG_VERSION}

python -m venv /venv
. /venv/bin/activate

export BORG_OPENSSL_PREFIX=/usr/local
export BORG_LIBZSTD_PREFIX=/usr/local
export BORG_LIBXXHASH_PREFIX=/usr/local
export BORG_LIBACL_PREFIX=/usr

# install all requirements
pip install -U pip wheel
# https://github.com/pyinstaller/pyinstaller/issues/6532#issuecomment-1021333333
pip install --no-binary pyinstaller pyinstaller
pip install -r requirements.d/development.txt
pip install -e .

# create standalone binary
pyinstaller --log-level DEBUG -F -n borg-${BORG_ARCH} src/borg/__main__.py

# link and output built borg binary version to ensure the binary works
ln -sf "${BORG_BUILD_DIR}/borg-${BORG_ARCH}" /usr/bin/borg
/usr/bin/borg --version