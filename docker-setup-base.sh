#!/usr/bin/env sh

BORG_ARCH=${1}
BORG_VERSION=${2:-1.4.0}

OPENSSL_VERSION=3.3.1
ZSTD_VERSION=1.5.6
XXHASH_VERSION=0.8.2

set -ex

# Change to archive sources
sed -i 's!http://deb.debian.org/debian!http://archive.debian.org/debian/!g' /etc/apt/sources.list

# And for Raspbian:
sed -i 's!http://archive.raspbian.org/!http://legacy.raspbian.org/!g' /etc/apt/sources.list

# install dev requirements
apt-get update || true
apt-get upgrade -y
apt-get install -y --no-install-recommends \
    build-essential git \
    fuse libfuse-dev pkg-config \
    libacl1-dev \
    zlib1g-dev liblz4-dev \
    libzstd-dev libffi-dev \
    libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev 
rm -rf /var/lib/apt/lists/*

cd /
# Install newer OpenSSL
curl https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz -o openssl.tar.gz -L
tar xvf openssl.tar.gz
cd openssl-$OPENSSL_VERSION
./config
make -j 2
make install
ldconfig /usr/local/lib

cd /
curl https://github.com/Cyan4973/xxHash/archive/refs/tags/v$XXHASH_VERSION.tar.gz -o xxHash.tar.gz -L
tar xvf xxHash.tar.gz
cd xxHash-$XXHASH_VERSION
make prefix=/usr/local
make prefix=/usr/local install


cd /
curl https://github.com/facebook/zstd/releases/download/v$ZSTD_VERSION/zstd-$ZSTD_VERSION.tar.gz -o zstd.tar.gz -L
tar xvf zstd.tar.gz
rm zstd.tar.gz
cd zstd-$ZSTD_VERSION
make prefix=/usr/local
make prefix=/usr/local install


cd /
export CONFIGURE_OPTS="--with-openssl=/usr/local"
curl https://pyenv.run | bash
export PATH="$HOME/.pyenv/bin:$HOME/.pyenv/shims:$PATH"
pyenv install --verbose 3.12
pyenv global 3.12
python3 -V