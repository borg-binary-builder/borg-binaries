#!/usr/bin/env sh
set -ev
chmod 600 $DEPLOY_SSH_KEY
sftp -o StrictHostKeyChecking=no -o IdentityFile=$DEPLOY_SSH_KEY -P 2222 borg-ci-runner@bauerj.eu <<EOF
put borg-${VERSION}-* /upload/ 
exit
EOF
