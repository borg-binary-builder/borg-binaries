#!/usr/bin/env sh

# Usage: ./build.sh <arch> [<version>] [<image>]

# For available architectures see:
# https://www.balena.io/docs/reference/base-images/base-images/#balena-base-images

set -ex

BORG_ARCH=${1}
BORG_VERSION=${2:-1.4.0}
PYTHON_VERSION=3.12

if [ -z "${BORG_ARCH}" ]; then
    echo "Usage: ./build.sh <arch> [<version>] [<image>]";
    echo "No arch provided";
    exit 1;
fi

docker system prune -af

CACHE_IMAGE="registry.gitlab.com/borg-binary-builder/borg-binaries:base-py$PYTHON_VERSION-$BORG_ARCH"

if docker pull "$CACHE_IMAGE" ; then
  echo "Using cached base image for Python $PYTHON_VERSION ($BORG_ARCH) 😊"
else
  echo "Building base image for Python $PYTHON_VERSION ($BORG_ARCH) 🕰️"
  docker build \
    --build-arg "FROM_IMAGE=balenalib/${BORG_ARCH}-python:3.8-stretch-run-20200221" \
    --build-arg "BORG_ARCH=${BORG_ARCH}" \
    --build-arg "BORG_VERSION=${BORG_VERSION}" \
    --build-arg "BUILD_SCRIPT=docker-setup-base.sh" \
    -t "$CACHE_IMAGE" .
    docker push "${CACHE_IMAGE}"
fi

docker build \
    --build-arg "FROM_IMAGE=$CACHE_IMAGE" \
    --build-arg "BORG_ARCH=${BORG_ARCH}" \
    --build-arg "BORG_VERSION=${BORG_VERSION}" \
    --build-arg "BUILD_SCRIPT=docker-setup.sh" \
    -t build .
